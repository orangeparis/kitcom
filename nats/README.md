
le backbone nats

ref https:nats.io


# Avantages

* solution open source apache.

* haute scalabilité horizontale
ajout simple et transparente de serveur sur le cluster

* construit pour la performance
 millions de messaeges par seconde par serveur

* intègre 3 topologies fondamentales

    ** publish/subscribe avec wild cards (.*)
  
    ** request / response avec timeout

    ** worker queues  ( load balancing )
  
* haute resilience , ( always on ) 
  déconnecte de façon claire les clients qui penalisent le fontionnement du core
  
* multi-tenant 
une seule infracture plusieurs "bus" totalement isolés
  
* Core NATS: 48 known client types 
  

* self-healing
tous les librairies clientes ( go / python / java ) communiquent avec les noeuds du cluster
  
pour eliminer les noeuds indisponibles et découvrir les nouveaux noeuds disponibles.
elimine le besoin de solutions lourdes style HA/CMP

* architecture cluster et super cluster
permet le fail over transparent et retour au nominal
  

* securite decentralisée avec 3 niveaux hierarchiques
 Organisation / Accompte / Utilisateur
  

* integration native avec mqtt ( iot internet des objets )

* plugins pour integration de mq-series , kafka , rabbit-mq etc ...

* Delivery Guarantees: 
  At most once, at least once, and exactly once is available in JetStream.