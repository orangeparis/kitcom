
# projet

supervision fonctionnelle des plate-formes TB1 et TB2 du train identité TGI

Chaque compossant du TIG est supervisé par un watcher qui effectue 
des requetes fonctionnelles et determine son état


# implementation

* watchers
* bus nats ( publish/subscribe)
* collecteur
* supervision ( prometheus,alertmanager,graffana )



Les "Watchers" sont constitués d'un ou plusieurs "Check" qui effectuent des requetes sur les composants
et publient des messages sur le bus nats

le message nats se compose de

## un "subject"
qui définit l'emetteur du message
le subject se décompose en 5 parties
le prefixe : "watcher"
la plateforme ( tb1/tb2 )
le composant ( idm/wassup ...)
le nom du watcher
le nom du check

eg : watcher.tb1.wassup.main.status

# un contenu

qui décrit un etat : OK KO
c'est un json 

eg: { "Result":1 } pour OK , {"Result":O"} KO



# le collecteur

le collecteur souscrit aux messages des watchers  watcher.>
et construit des metriques prometehus

# supervision
le serveur prometheus vient "scraper" les metriques sur le collecteur



# avantages nats

* maintenabilité
le watcher emet des messages en publish subscribe et peut etre testé isolément sans la presence 
  de toute la chaine de supervision
  
* observabilité 
on peut facilement voir l'ensemble des messages émis par les watchers
  
* transparence de localisation
simplicité de configuration : les watchers ne connaissent que le bus nats , ainsi que le collecteur
  
* genericité
le collecteur ne neccessite pas de modification lors de l'ajout de nouveaux watchers
  grace à la convention de nommage des "subject"
  
* scalabilité 
possibilité de transformer l'instance nats en cluster nats pour une scalabilité horizontale

* couplage faible
les watchers sont independant de la solution de supervision , qui peut evoluer sans impacter ceux-ci
  
